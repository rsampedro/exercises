"""
Given an array of integers, return a point where the sum of the integers to the left and the sum of the integers to the
right are equal.

Expected complexity = O(n)
"""
def EquilibriumCorrect(A):
    """
    This implementation is correct for a battery of tests, but it has a complexity of O(n^2) due to the use of sum
    inside of the while loop, leading to n^2 iterations over the array
    :param A: 
    :return: index of a position where the conditions apply or -1 in case it never happens
    """
    result = -1
    idx = 0
    while result == -1 and idx < len(A):
        left = A[:idx]
        right = A[idx+1:]

        if sum(left) == sum(right):
            result = idx
            #print idx

        idx += 1
    return result

def EquilibriumFast(A):
    """
    This implementation fails in some edge cases, but it has a complexity of O(n).
    Instead of travelling the array several times, we get the total sum of the values of the array and for every
    iteration we simply apply the changes to a pair of left and right values, which are the sums of each side.
    :param A: 
    :return: 
    """

    # Initial conditions
    right = sum(A)  # All the values are on the right side
    left = 0        # None are on the left

    idx = 0
    result = -1
    while result == -1 and idx < len(A):
        if idx > 0:
            left += A[idx-1]

        right -= A[idx]
        if left == right:
            result = idx
            #print idx
        idx+=1

    return result

if __name__ == '__main__':
    array = [-1, -2, -1, -2, 6]
    array2 = [-7, 1, 5, 2, -4, 3, 0]
    expected_result = 3

    eqCorrect = EquilibriumCorrect(array2)
    eqFast = EquilibriumFast(array2)

    print "Expected result == {}".format(expected_result)
    print "EquilibriumCorrect == {}, is expected result: {}".format(eqCorrect, expected_result==eqCorrect)
    print "EquilibriumFast == {}, is expected result: {}".format(eqFast, expected_result==eqFast)





