from PIL import Image


def hideTextInImage(image_route, text):
    """
    Hides a string into an image file, by replacing the least significant digit of each channel when represented as int,
    with each digit that composes the ascii value of the character to introduce
    :param image_route: Image route to the image to hide the text in
    :param text: String to hide inside the image
    :return: 
    """
    image = Image.open(image_route)
    arr = list(image.getdata())

    assert len(arr) > len(text)

    for idx, ch in enumerate(text):
        ascii_ch  = ord(ch)
        ascii_ch_str = str(ascii_ch).zfill(3)

        (r, g, b) = arr[idx]

        str_r = str(r)
        str_r = str_r[:-1]+ascii_ch_str[0]
        r = int(str_r)

        str_g = str(g)
        str_g = str_g[:-1] + ascii_ch_str[1]
        g = int(str_g)

        str_b = str(b)
        str_b = str_b[:-1] + ascii_ch_str[2]
        b = int(str_b)

        arr[idx] = (r,g,b)

    new_route = image_route + '_new.png'

    newimage = Image.new(image.mode, image.size)
    newimage.putdata(arr)

    newimage.save(new_route)
    newimage.close()


def readTextfromImage(route):
    text = ''
    image = Image.open(route)
    arr = list(image.getdata())

    end = False
    idx = 0
    while not end and idx < len(arr):
        char = ''

        pixel = arr[idx]
        char+=str(pixel[0]).zfill(3)[2]
        char+=str(pixel[1]).zfill(3)[2]
        char+=str(pixel[2]).zfill(3)[2]

        char = chr(int(char))
        if char == '\0':
            end = True

        text+=char
        idx += 1

    return text

def byteHideTextinImage(imageroute, text):
    """
    Hides a given string into the image specified by replacing the least significant bits in every channel 
    (R: 2, G:3, B:3) for each pixel by the corresponding bits of the ascii representation of the corresponding character
     of the string 
    :param image_route: Image route to the image to hide the text in
    :param text: String to hide inside the image
    :return: 
    """
    image = Image.open(imageroute)
    arr = list(image.getdata())

    assert len(arr) > len(text)

    for idx, ch in enumerate(text):

        ch_bin = format(ord(ch), 'b').zfill(8)

        (r, g, b) = arr[idx]

        bin_r = format(r, 'b')
        bin_str_r = str(bin_r).zfill(8)
        str_r = bin_str_r[:-2] + ch_bin[0:2]
        r = int(str_r, 2)

        bin_g = format(g, 'b')
        bin_str_g = str(bin_g).zfill(8)
        str_g = bin_str_g[:-3] + ch_bin[2:5]
        g = int(str_g, 2)

        bin_b = format(b, 'b')
        bin_str_b = str(bin_b).zfill(8)
        str_b = bin_str_b[:-3] + ch_bin[5:8]
        b = int(str_b, 2)

        arr[idx] = (r, g, b)

    new_route = imageroute + '_newbyte.png'

    newimage = Image.new(image.mode, image.size)
    newimage.putdata(arr)

    newimage.save(new_route)
    newimage.close()


def bytereadTextfromImage(route):
    text = ''
    image = Image.open(route)
    arr = list(image.getdata())

    end = False
    idx = 0
    while not end and idx < len(arr):
        char = ''

        pixel = arr[idx]
        bin_r = format(pixel[0], 'b')
        bin_g = format(pixel[1], 'b')
        bin_b = format(pixel[2], 'b')
        char+=bin_r[-2:]
        char+=bin_g[-3:]
        char+=bin_b[-3:]

        char = chr(int(char, 2))
        if char == '\0':
            end = True

        text+=char
        idx += 1

    return text





if __name__ == '__main__':

    image_route = '/home/rsampedro/Pictures/Lenna.png'
    jpeg_route = '/home/rsampedro/Pictures/photo_2017-05-04_09-34-24.jpg'
    image_route2 = '/home/rsampedro/Pictures/Lenna.png'+ '_newbyte.png'
    jpeg_route2 = '/home/rsampedro/Pictures/photo_2017-05-04_09-34-24.jpg' + '_newbyte.png'

    text = "This text is hidden inside the image\0"
    alttext = "What the fuck did you just fucking say about me, you little bitch? I'll have you know I graduated top of my class in the Navy Seals, and I've been involved in numerous secret raids on Al-Quaeda, and I have over 300 confirmed kills. I am trained in gorilla warfare and I'm the top sniper in the entire US armed forces. You are nothing to me but just another target. I will wipe you the fuck out with precision the likes of which has never been seen before on this Earth, mark my fucking words. You think you can get away with saying that shit to me over the Internet? Think again, fucker. As we speak I am contacting my secret network of spies across the USA and your IP is being traced right now so you better prepare for the storm, maggot. The storm that wipes out the pathetic little thing you call your life. You're fucking dead, kid. I can be anywhere, anytime, and I can kill you in over seven hundred ways, and that's just with my bare hands. Not only am I extensively trained in unarmed combat, but I have access to the entire arsenal of the United States Marine Corps and I will use it to its full extent to wipe your miserable ass off the face of the continent, you little shit. If only you could have known what unholy retribution your little \"clever\" comment was about to bring down upon you, maybe you would have held your fucking tongue. But you couldn't, you didn't, and now you're paying the price, you goddamn idiot. I will shit fury all over you and you will drown in it. You're fucking dead, kiddo.\0"
    alttext2 = "El miquel serio no mola una puta merda. Telegram si ho llegeixes\0"

    #hideTextInImage(image_route, alttext)
    #text = readTextfromImage(image_route2)

    #byteHideTextinImage(jpeg_route, alttext2)
    text = bytereadTextfromImage(jpeg_route2)



    pass