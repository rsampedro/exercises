<h1> Text in image </h1>

This script contains functions to hide and read text from the least significant characters of the pixels of a given
image.

This technique should be used by hiding the information in the lest significant <b>BYTES</b> 
for the pixels of the image, but I couldn't be bothered to do it yet and this will
only crash when you, by modifying a pixel put its value over 255, which will happen when
you try to write a pixel in the range 25x with a char value that has its 3rd character in the range 6-9